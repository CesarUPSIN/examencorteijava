package com.example.examencorteijava;

public class Rectangulo {
    private float base;
    private float altura;

    public Rectangulo() {
        this.base = 0.0f;
        this.altura = 0.0f;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getBase() {
        return this.base;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float getAltura() {
        return this.altura;
    }

    public float calcularArea() {
        return getBase() * getAltura();
    }

    public float calcularPerimetro() {
        return (getBase() * 2) + (getAltura() * 2);
    }
}
