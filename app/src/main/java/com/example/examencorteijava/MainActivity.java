package com.example.examencorteijava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView txtNombre;
    private Button btnEntrar, btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void iniciarComponentes() {
        txtNombre = findViewById(R.id.txtNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    public void entrar() {
        String strNombre = getResources().getString(R.string.nombre);

        if(txtNombre.getText().toString().equals(strNombre)) {
            Bundle bundle = new Bundle();
            bundle.putString("nombre", strNombre);


            Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Nombre no valido", Toast.LENGTH_LONG).show();
        }
    }
}