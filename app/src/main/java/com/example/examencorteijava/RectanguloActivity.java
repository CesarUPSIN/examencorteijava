package com.example.examencorteijava;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ResourceBundle;

public class RectanguloActivity extends AppCompatActivity {

    private TextView txtBase, txtAltura, txtArea, txtPerimetro;
    private Button btnCalcular, btnRegresar, btnLimpiar;
    Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rectangulo_main);

        iniciarComponentes();



        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }

    public void iniciarComponentes() {
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        txtArea = findViewById(R.id.txtArea);
        txtPerimetro = findViewById(R.id.txtPerimetro);


        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        rectangulo = new Rectangulo();
    }

    public void calcular() {
        if(validar() != -1) {
            rectangulo.setBase(Float.parseFloat(txtBase.getText().toString()));
            rectangulo.setAltura(Float.parseFloat(txtAltura.getText().toString()));

            txtArea.setText(String.valueOf(rectangulo.calcularArea()));
            txtPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
        }
    }

    public void limpiar() {
        txtBase.setText("");
        txtAltura.setText("");
        txtArea.setText("");
        txtPerimetro.setText("");
    }

    public int validar() {
        if(txtBase.getText().toString().matches("")||txtAltura.getText().toString().matches("")) {
            Toast.makeText(getApplicationContext(), "Faltan datos", Toast.LENGTH_LONG).show();
            return -1;
        }
        return 0;
    }

    public void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Rectangulo");
        confirmar.setMessage("¿Deseas regresar?");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {}).show();

    }
}
